import DS from 'ember-data';

var Customer = DS.Model.extend({
  number:    DS.attr('number'),
  name:      DS.attr('string')
});

Customer.reopenClass({
  FIXTURES: [
    {
      id: 1,
      number: 88,
      name: "Samson Ootoovak"
    },
    {
      id: 2,
      number: 25,
      name: "Samwise Gamgee"
    },
    {
      id: 3,
      number: 7,
      name: "Toucan Sam"
    }
  ]
});

export default Customer;
